#!/usr/bin/env python
# coding: utf-8

# # Prediction of students score based on study hours.
# #By- Aarush Kumar
# #Dated: May 24,2021

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats as stats
import statsmodels.formula.api as smf
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier


# In[3]:


df=pd.read_csv(r"/home/aarush100616/Downloads/Projects/Student Grade Prediction/scores.csv")
df


# ## Data Exploration

# In[4]:


df.shape


# In[5]:


df.columns


# In[6]:


df.info


# In[7]:


df.describe()


# In[8]:


df.corr()


# In[9]:


df.isnull()


# ## Data visualization

# In[10]:


sns.distplot(df["Scores"])
plt.show()


# In[11]:


sns.distplot(df["Scores"], kde=False, rug=True)
plt.show()


# In[12]:


#Joint plot
sns.jointplot(df['Hours'], df['Scores'], kind = "reg").annotate(stats.pearsonr)
plt.show()


# In[13]:


#Scores and hours correlated
sns.regplot(x="Hours", y="Scores", data=df)
plt.title("Plotting the regression line")


# In[14]:


#Simple Linear Regression
X = df.iloc[:, :-1].values
y = df.iloc[:, -1].values


# In[15]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.30, random_state = 0)


# In[16]:


from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)


# In[17]:


y_pred = regressor.predict(X_test)
y_pred


# In[18]:


#comparing actual vs predicted
df1 = pd.DataFrame({'Actual': y_test, 'Predicted_Score': y_pred})  
df1


# In[19]:


# PLotting the training set
plt.scatter(X_train,y_train, color='blue')
plt.plot(X_train,regressor.predict(X_train),color='red')
plt.title('(Trainig set)')
plt.xlabel('Hours')
plt.ylabel('Scores')
plt.show()


# In[20]:


#Calculating coeffeciants of the simple linear regression equation: y = C0 + C1.x (C1: Is the Slope, C0:Is the Intercept)
mean_x = np.mean(df['Hours'])
mean_y = np.mean(df['Scores'])
num = 0
den = 0
x = list(df['Hours'])
y = list(df['Scores'])
for i in range(len(df)):
    num += (x[i]-mean_x)*(y[i]-mean_y)
    den += (x[i]-mean_x)**2
B1 = num/den


# In[21]:


B0 = mean_y - B1*mean_x


# In[22]:


df['predicted_Scores'] = B0 + B1*df['Hours']
df.head()


# In[23]:


plt.scatter(df['Hours'], df['Scores'], c='red', label='Aactual Marks')
plt.scatter(df['Hours'], df['predicted_Scores'], c='blue', label='Predected Marks')
plt.title('Actual scores bw predicted scores')
plt.xlabel('Hours')
plt.ylabel('Scores')
plt.plot()


# In[24]:


y = B0 + B1*9.25
print("Marks scored by the student who study 9.25 hours a day is ",y)


# In[25]:


#Determining whether student passed or failed.
# Lets the cut of be 40 marks
cut_off = 40
df['Result'] = df['Scores']>=40
df


# In[26]:


df["Result"] = df["Result"].astype(str)
df.Result = df.Result.replace({"True": "Passed", "False": "Failed"})
df


# In[27]:


df["Result"].value_counts()


# In[28]:


Results = ['Passed', 'Failed']
data = [15,10]
explode = (0.1, 0.0)
colors = ( "orange", "blue")
wp = { 'linewidth' : 1, 'edgecolor' : "green" } 


# In[29]:


# Creating autocpt arguments 
def func(pct, allvalues): 
    absolute = int(pct / 100.*np.sum(allvalues)) 
    return "{:.1f}%\n({:d} g)".format(pct, absolute) 


# In[33]:


# Creating plot 
fig, ax = plt.subplots(figsize =(15, 10)) 
wedges, texts, autotexts = ax.pie(data,  
                                  autopct = lambda pct: func(pct, data), 
                                  explode = explode,  
                                  labels = Results, 
                                  shadow = True, 
                                  colors = colors, 
                                  startangle = 90, 
                                  wedgeprops = wp, 
                                  textprops = dict(color ="black")) 
# Adding legend 
ax.legend(wedges, Results, 
          title ="Results", 
          loc ="center left", 
          bbox_to_anchor =(1, 0)) 
  
plt.setp(autotexts, size = 8, weight ="bold") 
ax.set_title("Students Results") 
plt.show()

