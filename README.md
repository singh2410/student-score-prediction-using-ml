# Student Score Prediction using ML
## By-Aarush Kumar
Student Score Prediction using LinearRegression and prediction whether student passed or not.
In this model I have used LinearRegression in order to make predictions.The model calculates the marks and pass-fail criteria on basis of hours a student study and considering a particular minimum marks required by that student to pass the exam.
## Steps followed:
### Data loading-
![Screenshot_from_2021-05-25_06-03-53](/uploads/98ff8387dd7667ddfd833920e86ce6c2/Screenshot_from_2021-05-25_06-03-53.png)
### Data Exploration-
![Screenshot_from_2021-05-25_06-04-10](/uploads/97e643ab40409da468cc2240c8a61823/Screenshot_from_2021-05-25_06-04-10.png)
### Data visualization-
![Screenshot_from_2021-05-25_06-04-43](/uploads/bdc9efcd2bec39b641b776f883b9ea64/Screenshot_from_2021-05-25_06-04-43.png)
### Train and test model-
![Screenshot_from_2021-05-25_06-05-14](/uploads/0d0faaadf4f12389330aae0a7cf02911/Screenshot_from_2021-05-25_06-05-14.png)
### Plotting the results-
![Screenshot_from_2021-05-25_06-05-29](/uploads/c3ed992ac3022675288c3cab7fdd5a24/Screenshot_from_2021-05-25_06-05-29.png)
## Thankyou!
